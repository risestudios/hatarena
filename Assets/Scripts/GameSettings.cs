using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drafts;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class GameSettings : MonoBehaviour
{

    void Start() {
        
    }

    void Update() {
        if (Input.GetButtonDown("Cancel")) {
            QuitGameRequest();
        }
        if (Input.GetKeyDown(KeyCode.P)){
            SceneManager.LoadSceneAsync(0);
        }
    }

    public void QuitGameRequest() { StartCoroutine(QuitRequest()); }

    public IEnumerator QuitRequest()
    {
        yield return UserInput.Choice01("Deseja <b>Fechar</b> o jogo?");
        if (UserInput.accepted)
            Application.Quit();
    }

    public void QualquerCoisa()
    {
        
    }

}
