using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowAxis : MonoBehaviour
{
    public bool followX, followY;
    public Transform target;
    void Update()
    {
        if (!target) return;
        var pos = new Vector2(
            followX ? target.position.x : transform.position.x,
            followY ? target.position.y : transform.position.y
            );
        transform.position = pos;
    }
}
