﻿using Drafts;
using HatArena.Arena;
using System;
using UnityEngine;

namespace HatArena.Arena {

	public abstract class CharacterAction {

		protected Character character;
		protected string animationName;
		protected CharacterAction nextAction;
		protected int inputDelay;
		protected bool CanInput => (inputDelay -= 1) < 0;

		AnimatorStateInfo animation => character.animator.GetCurrentAnimatorStateInfo(0);

		protected CharacterAction(Character cha, string anim) {
			character = cha;
			animationName = anim;
		}

		public void Start() {
			character.animator.Play(animationName);
			OnStart();
			character.currentAction = this;
			nextAction = null;
			inputDelay = -1;
		}
		protected virtual void OnStart() { }

		public virtual void Update() {
			OnUpdate();
			ChainOnAnimationEnd(nextAction);
		}

		protected void ChainOnAnimationEnd(CharacterAction chain) {
			if(animation.normalizedTime >= 1) // fim da animaçao
				if(chain != null) chain.Start(); // comba com outra açao ao terminar a animaçao
				else character.actions.idle.Start(); // volta pro idle
		}

		protected virtual void OnUpdate() { }
	}
}

namespace HatArena.ArenaActions {

	public class Idle : CharacterAction {
		public Idle(Character cha) : base(cha, "Idle") { }

		protected override void OnStart() {
			character.movement.run = false;
		}

		public override void Update() {
			// movimento
			character.movement.SetTargetNormalizedSpeed(character.input.MoveDirection);
			if(character.input.JumpPressed) character.movement.Jump();
			if(character.input.DownPressed) character.movement.GoDown();

			// corrida
			if(Input.GetKeyUp(character.input.leftKey) || Input.GetKeyUp(character.input.leftKey))
				character.movement.run = false;
			if(character.input.RunTriggered) character.movement.run = true;

			// açoes
			if(character.input.GuardPressed) character.actions.guard.Start();
			if(character.input.SpecialPressed) character.actions.special.Start();
			if(character.input.UltimatePressed) character.actions.ultimate.Start();
			if(character.input.AttackPressed) (character.movement.inGround
					? (character.movement.run
						? character.actions.quick
						: character.actions.attack1)
					: character.actions.aerial).Start();
		}
	}

	public class Damaged : CharacterAction {
		float interruptTime;
		Timer timer = new Timer(false);

		public Damaged(Character cha, float interruptTime) : base(cha, "Damaged") {
			this.interruptTime = interruptTime;
		}

		protected override void OnStart() {
			timer.Reset();
		}

		public override void Update() {
			if(timer[interruptTime])
				character.actions.idle.Start();
		}
	}

	public class Guard : CharacterAction {
		public Guard(Character cha) : base(cha, "Block") { }

		public override void Update() {
			if(!Input.GetKey(character.input.guardKey))
				character.actions.idle.Start();
		}
	}

	public abstract class Attack : CharacterAction {
		float knockback;
		int damage;
		float slideSpeed;
		Func<Character.Actions, CharacterAction> getChainAction;

		protected Attack(Character cha, string anim, int dmg, float knbk, float slide,
		Func<Character.Actions, CharacterAction> getChain = null) : base(cha, anim) {
			damage = dmg;
			knockback = knbk;
			slideSpeed = slide;
			getChainAction = getChain;
		}

		protected override void OnStart() {
			character.stats.damage = damage;
			character.stats.knockback = knockback;
		}

		protected override void OnUpdate() {
			character.movement.ForceNormalizedSpeed(Vector2.right * character.movement.Side * slideSpeed);

			if(CanInput && character.input.AttackPressed)
				nextAction = getChainAction?.Invoke(character.actions);
		}
	}

	public class Attack1 : Attack {
		public Attack1(Character cha) : base(cha, "Attack1", 5, 1, 1 / 4f, a => a.attack2) { }
	}
	public class Attack2 : Attack {
		public Attack2(Character cha) : base(cha, "Attack2", 5, 1, 1 / 4f, a => a.attack3) { }
	}
	public class Attack3 : Attack {
		public Attack3(Character cha) : base(cha, "Attack3", 10, 1, 1 / 4f) { }
	}
	public class QuickAttack : Attack {
		public QuickAttack(Character cha) : base(cha, "RunAttack", 8, 1, 2) { }
	}

	/// <summary>Chute Aereo, encerra ao tocar no chao.</summary>
	public class Aerial : Attack {
		public Aerial(Character cha) : base(cha, "JumpAttack", 5, 1, 0) { }

		public override void Update() {
			//character.movement.SetTargetNormalizedSpeed(character.input.MoveDirection);

			if(character.movement.inGround)
				character.actions.idle.Start();
		}
	}
}
