﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactinoAtk : MonoBehaviour
{
    Rigidbody2D rb;
    public float forceJump;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
            HitJump();
    }

    void HitJump()
    {
        rb.AddForce(new Vector2 (0,forceJump));
    }
    
}
