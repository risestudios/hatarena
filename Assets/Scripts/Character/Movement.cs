﻿using UnityEngine;
using Photon.Pun;
using Drafts;

namespace HatArena.Arena {

	[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
	public class Movement : MonoBehaviour {

		[Header("Components")]
		public Animator animator;
		public SpriteRenderer spriteRenderer;
		[HideInInspector] new public Rigidbody2D rigidbody;
		[HideInInspector] new public Collider2D collider;
		[HideInInspector] public PhotonView photonView;

		[Header("Movement Variables")]
		[SerializeField] float speed;
		[SerializeField] float runSpeed;
		[SerializeField] float jumpSpeed;
		[SerializeField] float acceleration;
		[SerializeField] float groundDistance;
		[SerializeField] ContactFilter2D groundLayer;
		[SerializeField] int NormalLayer;
		[SerializeField] int PlatformLayer;

		float targetSpeed;
		public bool run;
		Timer goThroughTimer;

		//[SerializeField] float gravity;
		[ReadOnly] public bool inGround;
		[ReadOnly] public bool inWall;
		public bool isJumping => rigidbody.velocity.y > 0;
		public bool isFalling => rigidbody.velocity.y < 0;

		public bool canWalk, canJump;

		[Header("Unlockable Moviments")]
		public bool canDoubleJump;
		bool didDoubleJump;
		public bool canWallJump;

		public float Side {
			get => spriteRenderer.flipX ? -1 : 1;
			set => spriteRenderer.flipX = value != 0 ? value < 0 : spriteRenderer.flipX;
		}

		void Awake() {
			rigidbody = GetComponent<Rigidbody2D>();
			animator = GetComponent<Animator>();
			collider = GetComponent<Collider2D>();
			photonView = GetComponent<PhotonView>();

			goThroughTimer = new Timer(false) {
				onTrigger = () => gameObject.layer = NormalLayer,
				onReset = () => gameObject.layer = PlatformLayer
			};
			goThroughTimer.Trigger();
		}

		public void SetTargetNormalizedSpeed(float normalizedSpeed, bool? forceRun = null) {
			if(!photonView.IsMine) return;
			targetSpeed = normalizedSpeed * ((forceRun ?? run) ? runSpeed : speed);
		}

		public void ForceNormalizedSpeed(Vector2 velocity, bool? forceRun = false) {
				targetSpeed = velocity.x * (forceRun ?? run ? runSpeed : speed);
			rigidbody.velocity = new Vector2(targetSpeed, velocity.y);
		}

		void Update() => goThroughTimer.Update(.5f);

		void FixedUpdate() {
			// ground & wall check
			RaycastHit2D[] hits = new RaycastHit2D[1];
			inGround = collider.Cast(Vector2.down, groundLayer, hits, groundDistance) > 0;
			inGround = inGround && !goThroughTimer.IsPlaying;
			inWall = collider.Cast(Vector2.right * Side, groundLayer, hits, groundDistance) > 0;
			inWall = inWall && !inGround && !isJumping;

			// animator state
			animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));
			animator.SetBool("Jump", !inGround && !inWall);
			//animator.SetBool("Wall", inWall);

			//if(!photonView.IsMine) return;

			// Smooth Movement
			var xspd = Mathf.MoveTowards(rigidbody.velocity.x, targetSpeed, acceleration);
			//var yspd = inGround && isFalling ? 0 : rigidbody.velocity.y - gravity * Time.deltaTime;
			var yspd = rigidbody.velocity.y;
			rigidbody.velocity = new Vector2(canWalk ? xspd : 0, yspd);
			Side = rigidbody.velocity.x;

			if(inGround || inWall) didDoubleJump = false;
		}

		public void Jump() {
			if((!inGround && !inWall && didDoubleJump) || !canJump) return;
			if(inWall && canWallJump) rigidbody.velocity = new Vector2(-0.5f * Side, jumpSpeed); // recoil
			else rigidbody.velocity = new Vector2(rigidbody.velocity.x, jumpSpeed); // normal/double jump
			if(!inGround) didDoubleJump = true;
		}

		public void GoDown() => goThroughTimer.Reset();
	}

}
