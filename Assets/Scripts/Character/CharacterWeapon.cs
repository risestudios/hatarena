﻿using UnityEngine;

namespace HatArena.Arena {

	[RequireComponent(typeof(Collider2D))]
	public class CharacterWeapon : MonoBehaviour {
		public Character character;

		void OnCollisionEnter2D(Collision2D collision) {
			if(collision.collider.TryGetComponent<Character>(out var target) && target != character) {
				target.photonView.RPC("TakeDamage", Photon.Pun.RpcTarget.All, character.stats.damage);
				Instantiate(PrefabShortcut.instance.NormalHit, collision.GetContact(0).point, Quaternion.identity, transform);
			}
		}
	}
}