using Photon.Pun;
using UnityEngine;

namespace HatArena.Arena {

	[RequireComponent(typeof(Movement), typeof(CharacterStats))]
	[RequireComponent(typeof(ControllerInput), typeof(PhotonView))]
	public partial class Character : MonoBehaviour {

		[Header("Components")]
		public Animator animator;
		[HideInInspector] public Movement movement;
		[HideInInspector] public CharacterStats stats;
		[HideInInspector] public ControllerInput input;
		[HideInInspector] public PhotonView photonView;

		public CharacterAction currentAction;
		public bool isGuarding;

		// Start is called before the first frame update
		void Awake() {
			movement = GetComponent<Movement>();
			stats = GetComponent<CharacterStats>();
			input = GetComponent<ControllerInput>();
			photonView = GetComponent<PhotonView>();

			if(!photonView.IsMine) return;
			actions = new Actions(this);
			actions.idle.Start();

			PlayerVirtualCamera.vcamera.Follow = transform;

			// color
			movement.spriteRenderer.color = new Color(PlayerPrefs.GetFloat("ColorR", 1), PlayerPrefs.GetFloat("ColorG", 1), PlayerPrefs.GetFloat("ColorB", 1), 1);
			//if(ColorUtility.TryParseHtmlString(PlayerPrefs.GetString("Color", "none"), out var color))
			//	movement.spriteRenderer.color = color;
		}

		// Update is called once per frame
		void Update() {
			if(!photonView.IsMine) return;
			currentAction.Update();
		}

		// A�oes do personagem
		public Actions actions;
		public class Actions {

			public CharacterAction idle, guard;
			public CharacterAction damaged, trown, dying;
			public CharacterAction attack1, attack2, attack3;
			public CharacterAction quick, aerial;
			public CharacterAction special, ultimate;

			public Actions(Character character) {
				idle = new ArenaActions.Idle(character);
				guard = new ArenaActions.Guard(character);

				damaged = new ArenaActions.Damaged(character, .2f);
				trown = new ArenaActions.Idle(character);
				dying = new ArenaActions.Idle(character);

				attack1 = new ArenaActions.Attack1(character);
				attack2 = new ArenaActions.Attack2(character);
				attack3 = new ArenaActions.Attack3(character);

				quick = new ArenaActions.QuickAttack(character);
				aerial = new ArenaActions.Aerial(character);

				special = new ArenaActions.Attack3(character);
				ultimate = new ArenaActions.Attack3(character);
			}
		}

		[PunRPC]
		public void TakeDamage(int damage) {
			if(isGuarding && stats.Guard > 0) stats.Guard -= damage;
			else if(stats.HP > 0) {
				if(stats.HP <= 0) {
					stats.HP = 0;
					Death2();
				}
			}
			currentAction = actions.damaged;
		}

		void Death2() {
			Destroy(gameObject, 0.2f);
		}
	}
}