﻿using UnityEngine.UI;
using TMPro;
using Drafts.Components;

namespace HatArena.Arena {
	public class CharacterStatsUI : MonoPool<CharacterStatsUI> {
		public TextMeshProUGUI nameText;
		public Slider healthBar;
		public Slider guardBar;
		public Slider kiBar;

		private void Start() => gameObject.SetActive(false);
	}
}
