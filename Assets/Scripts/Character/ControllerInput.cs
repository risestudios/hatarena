﻿using Drafts;
using UnityEngine;
using UnityEngine.UI;

namespace HatArena.Arena {

	public class ControllerInput : MonoBehaviour {

		[Header("PC Inputs")]
		public KeyCode leftKey;
		public KeyCode rightKey;
		public KeyCode downKey;
		public KeyCode jumpKey;
		public KeyCode attackKey;
		public KeyCode guardKey;
		public KeyCode specialKey;
		public KeyCode ultimateKey;

		[Header("Mobile Inputs")]
		public GameObject controllerUI;
		//public Joystick joystick;
		public Button jumpButton;
		public Button runButton;
		public Button guardButton;
		public Button attackButton;
		public Button specialButton;
		public Button ultimateButton;

		Timer runTimer = new Timer(false);

#if UNITY_ANDROID
		public float MoveDirection { get; private set; }
		public bool JumpPressed { get; private set; }
		public bool AttackPressed { get; private set; }
		public bool GuardPressed { get; private set; }
		public bool SpecialPressed { get; private set; }
		public bool UltimatePressed { get; private set; }
		public bool RunTriggered { get; private set; }

		public void Awake() {
			controllerUI.SetActive(true);
			jumpButton.onClick.AddListener(() => JumpPressed = true);
			runButton.onClick.AddListener(() => RunTriggered = true);
			guardButton.onClick.AddListener(() => GuardPressed = true);
			attackButton.onClick.AddListener(() => AttackPressed = true);
			specialButton.onClick.AddListener(() => SpecialPressed = true);
			ultimateButton.onClick.AddListener(() => UltimatePressed = true);
		}
#else
		public float MoveDirection => (Input.GetKey(leftKey) ? -1 : 0) + (Input.GetKey(rightKey) ? 1 : 0);
		public bool DownPressed => Input.GetKeyDown(downKey);
		public bool JumpPressed => Input.GetKeyDown(jumpKey);
		public bool AttackPressed => Input.GetKeyDown(attackKey);
		public bool GuardPressed => Input.GetKeyDown(guardKey);
		public bool SpecialPressed => Input.GetKeyDown(specialKey);
		public bool UltimatePressed => Input.GetKeyDown(ultimateKey);

		public bool RunTriggered {
			get {
				if(Input.GetKeyDown(leftKey) || Input.GetKeyDown(rightKey)) {
					if(runTimer.IsPlaying) {
						runTimer.Reset();
						return true;
					}
					runTimer.Reset();
				}
				return false;
			}
		}
#endif

		private void Update() {
			runTimer.Update(.2f);

#if UNITY_ANDROID
			JumpPressed = false;
			RunTriggered = false;
			GuardPressed = false;
			AttackPressed = false;
			SpecialPressed = false;
			UltimatePressed = false;
#endif
		}
	}
}