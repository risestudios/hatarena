﻿using Cinemachine;
using Drafts.Components;
using UnityEngine;

namespace HatArena.Arena {
	[RequireComponent(typeof(CinemachineVirtualCamera))]
	public class PlayerVirtualCamera : MonoSingle<PlayerVirtualCamera> {
		public static CinemachineVirtualCamera vcamera => Instance.GetComponent<CinemachineVirtualCamera>();
	}
}