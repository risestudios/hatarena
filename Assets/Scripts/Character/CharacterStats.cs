﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

namespace HatArena.Arena {

	[RequireComponent(typeof(PhotonView))]
	public class CharacterStats : MonoBehaviour {

		[SerializeField] Canvas infoCanvas;
		[SerializeField] GameObject arrowIndicator;
		[SerializeField] TextMeshProUGUI nameText;
		[SerializeField] Slider healthBar;
		[SerializeField] Slider guardBar;

		CharacterStatsUI infoUI;

		public int maxHp, maxGuard, maxKi;
		[HideInInspector] public float ki;
		[HideInInspector] public int damage;
		[HideInInspector] public float knockback;

		public int HP {
			get => (int)healthBar.value;
			set {
				healthBar.value = value;
				if(infoUI) infoUI.healthBar.value = value;
			}
		}

		public float Guard {
			get => guardBar.value;
			set {
				guardBar.value = value;
				if(infoUI) infoUI.guardBar.value = value;
			}
		}

		public float Ki {
			get => ki;
			set {
				ki = value;
				if(infoUI) infoUI.kiBar.value = value;
			}
		}

		private void Awake() {
			infoCanvas.worldCamera = Camera.main;

			var isMe = GetComponent<PhotonView>().IsMine;
			if(isMe) infoUI = CharacterStatsUI.Instances[0];

			arrowIndicator.SetActive(isMe);
			nameText.gameObject.SetActive(!isMe);
			healthBar.gameObject.SetActive(!isMe);
			guardBar.gameObject.SetActive(!isMe);

			nameText.text = PlayerPrefs.GetString("PlayerName", "Player");
			if(infoUI) infoUI.nameText.text = nameText.text;

			if(infoUI) infoUI.healthBar.maxValue = maxHp;
			healthBar.maxValue = maxHp;
			HP = maxHp;

			if(infoUI) infoUI.guardBar.maxValue = maxGuard;
			guardBar.maxValue = maxGuard;
			Guard = maxGuard;

			if(infoUI) infoUI.kiBar.maxValue = maxKi;
			Ki = maxKi;
		}
	}
}
