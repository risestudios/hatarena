using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxManager : MonoBehaviour
{
    public Transform target;
    float previousPos;
    public Renderer BgFar, BgMedium, BgClose;
    public float paralaxSpeedFar, paralazSpeedMedium, paralaxSpeedClose;
    void Update() {
        float delta = target.position.x - previousPos;
        if (target.position.x != previousPos)
            previousPos = target.position.x;
        BgFar.material.mainTextureOffset += new Vector2(delta * paralaxSpeedFar * Time.deltaTime, 0);
        BgMedium.material.mainTextureOffset += new Vector2(delta * paralazSpeedMedium * Time.deltaTime, 0);
        BgClose.material.mainTextureOffset += new Vector2(delta * paralaxSpeedClose * Time.deltaTime, 0);
    }
}
