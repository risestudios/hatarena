﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabShortcut : MonoBehaviour
{
    public static PrefabShortcut instance;
    public GameObject NormalHit;
    public GameObject SpecialHit;
    
    void Awake ()
    {
        instance = this;
    }
}
