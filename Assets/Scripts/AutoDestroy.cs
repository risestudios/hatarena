﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    [Range(0,1)]
    public float timeToDestroy;
    void Start()
    {
        Destroy(this.gameObject,timeToDestroy);
    }
}
