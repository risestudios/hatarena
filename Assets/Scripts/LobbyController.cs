﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Drafts;
using ExitGames.Client.Photon;

public class LobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject startButton;
    [SerializeField]
    private GameObject cancelButton;
    [SerializeField]
    private int roomSize;
    [SerializeField]
    private TMP_InputField nameField;
    private string currentColor;

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        nameField.interactable = true;
        nameField.text = PlayerPrefs.GetString("PlayerName", "");
        currentColor = PlayerPrefs.GetString("PlayerColor", "#FFFFFF");
        if (nameField.text != "")
            startButton.SetActive(true);
    }

    public void StartButton()
    {
        startButton.SetActive(false);
        cancelButton.SetActive(true);
        Hashtable player = new Hashtable();
        player.Add("PlayerName", nameField.text);
        player.Add("PlayerColor", currentColor);
        PhotonNetwork.LocalPlayer.SetCustomProperties(player);
        PhotonNetwork.JoinRandomRoom();
    }

    public void Cancel()
    {
        //nameField.interactable = true;
        //startButton.SetActive(true);
        cancelButton.SetActive(false);
        PhotonNetwork.LeaveRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        CreateRoom();
    }

    void CreateRoom()
    {
        int randomRoomNumber = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)roomSize };
        PhotonNetwork.CreateRoom("Room " + randomRoomNumber, roomOps);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        CreateRoom();
    }

    public void ActiveStartButton()
    {
        //nameField.interactable = false;
        if (!startButton.activeSelf)
            startButton.SetActive(true);
        if (nameField.text == "")
            PlayerPrefs.SetString("PlayerName", "Player");
        else
            PlayerPrefs.SetString("PlayerName", nameField.text);
    }

    public void SelectPlayerColor(Button c) {
        PlayerPrefs.SetString("PlayerColor","#" + ColorUtility.ToHtmlStringRGBA(c.image.color));
        currentColor = "#" + ColorUtility.ToHtmlStringRGBA(c.image.color);
    }

}
